Name:	 	libtraceevent
Version: 	1.8.4
Release: 	2
License: 	LGPLv2+ and GPLv2+
Summary: 	Library to parse raw trace event formats

URL: 	       	https://git.kernel.org/pub/scm/libs/libtrace/libtraceevent.git/
Source0:       	https://git.kernel.org/pub/scm/libs/libtrace/libtraceevent.git/snapshot/libtraceevent-%{version}.tar.gz

Patch0000:      0001-fix-missing-fstack-protector-strong.patch

BuildRequires: 	gcc
BuildRequires: 	xmlto
BuildRequires: 	asciidoc

Conflicts:      perf < 6.2.0

%global __provides_exclude_from ^%{_libdir}/traceevent/plugins

%description
libtraceevent is a library to parse raw trace event formats.

%package devel
Summary: Development headers of %{name}
Requires: %{name}%{_isa} = %{version}-%{release}

%description devel
Development headers of %{name}-libs

%prep
%autosetup  -n %{name}-%{version} -p1

%build
MANPAGE_DOCBOOK_XSL=`rpm -ql docbook-style-xsl | grep manpages/docbook.xsl`
# Parallel build does not work
make -O -j1 V=1 VERBOSE=1 CFLAGS="%{build_cflags}" LDFLAGS="%{build_ldflags}" prefix=%{_prefix} libdir=%{_libdir} MANPAGE_XSL=%{MANPAGE_DOCBOOK_XSL} all doc

%install
%make_install prefix=%{_prefix} libdir=%{_libdir} install doc-install
rm -rf %{buildroot}/%{_libdir}/libtraceevent.a

%files
%license LICENSES/LGPL-2.1
%license LICENSES/GPL-2.0
%{_libdir}/traceevent/
%{_libdir}/libtraceevent.so.%{version}
%{_libdir}/libtraceevent.so.1
%{_mandir}/man3/tep_*.3*
%{_mandir}/man3/libtraceevent.3*
%{_mandir}/man3/trace_seq*.3*
%{_mandir}/man3/kbuffer_*.3*
%{_docdir}/%{name}-doc

%files devel
%{_includedir}/traceevent/
%{_libdir}/libtraceevent.so
%{_libdir}/pkgconfig/libtraceevent.pc

%changelog
* Wed Dec 25 2024 xu_ping <707078654@qq.com> - 1.8.4-2
- add conflict perf version.

* Mon Dec 02 2024 xu_ping <707078654@qq.com> - 1.8.4-1
- update to 1.8.4
  * Add meson build targets to Makefile
  * Close shared object in the error path of load_plugin()
  * prevent a memory leak in tep_plugin_add_option()
  * Prevent a memory leak in process_fields()

* Sun Jun 30 2024 shafeipaozi <sunbo.oerv@isrc.iscas.ac.cn> - 1.8.2-1
- update to 1.8.2

* Thu Jul 13 2023 liyanan <thistleslyn@163.com> - 1.2.1-4
- Delete confilt with perf when the kernel is greater than 6.2

* Thu Jan 19 2023 yaoxin <yaoxin30@h-partners.com> - 1.2.1-3
- Add conflict with perf

* Mon Dec 05 2022 xu_ping <xuping33@h-partners.com> - 1.2.1-2
- Add -fstack-protector-strong compile options

* Fri Jan 14 2022 houyingchao <houyingchao@huawei.com> - 1.2.1-1
- Init package

